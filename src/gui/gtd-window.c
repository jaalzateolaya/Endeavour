/* gtd-window.c
 *
 * Copyright (C) 2015-2020 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 * Copyright (C) 2022-2023 Jamie Murphy <hello@itsjamie.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "GtdWindow"

#include "config.h"

#include "gtd-application.h"
#include "gtd-debug.h"
#include "gtd-task-list-view.h"
#include "gtd-manager.h"
#include "gtd-notification.h"
#include "gtd-provider.h"
#include "gtd-provider-popover.h"
#include "gtd-panel.h"
#include "gtd-panel-all-tasks.h"
#include "gtd-panel-inbox.h"
#include "gtd-panel-next-week.h"
#include "gtd-panel-task-list.h"
#include "gtd-panel-today.h"
#include "gtd-sidebar.h"
#include "gtd-task.h"
#include "gtd-task-list.h"
#include "gtd-window.h"

#include <glib/gi18n.h>

struct _GtdWindow
{
  AdwApplicationWindow application;

  GtkWidget          *back_button;
  GtkWidget          *content_box;
  GtkMenuButton      *gear_menu_button;
  AdwLeaflet         *leaflet;
  GtkWidget          *new_list_button;
  GtkMenuButton      *primary_menu_button;
  GtkStack           *stack;
  GtdSidebar         *sidebar;
  GtkWidget          *sidebar_box;

  AdwToastOverlay    *notification_overlay;

  GtdPanel           *active_panel;
  GtdPanel           *task_list_panel;

  GHashTable         *notification_list;

  GList              *panels_set;
  GSimpleActionGroup *action_group;
};

typedef struct
{
  GtdWindow          *window;
  gchar              *primary_text;
  gchar              *secondary_text;
} ErrorData;

G_DEFINE_FINAL_TYPE (GtdWindow, gtd_window, ADW_TYPE_APPLICATION_WINDOW)

enum
{
  PANEL_ADDED,
  PANEL_REMOVED,
  NUM_SIGNALS
};

static guint signals[NUM_SIGNALS] = { 0, };

/*
 * Auxiliary methods
 */

static void
error_data_free (ErrorData *error_data)
{
  g_free (error_data->primary_text);
  g_free (error_data->secondary_text);
  g_free (error_data);
}

static void
setup_development_build (GtdWindow *self)
{
  g_message (_("This is a development build of Endeavour. You may experience errors, wrong behaviors, "
               "and data loss."));

  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
}

static gboolean
is_development_build (void)
{
#ifdef DEVELOPMENT_BUILD
  return TRUE;
#else
  return FALSE;
#endif
}

static void
load_geometry (GtdWindow *self)
{
  GSettings *settings;
  GtkWindow *window;
  gboolean maximized;
  gint height;
  gint width;

  window = GTK_WINDOW (self);
  settings = gtd_manager_get_settings (gtd_manager_get_default ());

  maximized = g_settings_get_boolean (settings, "window-maximized");
  g_settings_get (settings, "window-size", "(ii)", &width, &height);

  gtk_window_set_default_size (window, width, height);

  if (maximized)
    gtk_window_maximize (window);
}

static void
add_panel (gpointer data,
           gpointer user_data)
{
  GtdPanel *panel = GTD_PANEL (data);
  GtdWindow *self = GTD_WINDOW (user_data);

  gtk_stack_add_titled (self->stack,
                        GTK_WIDGET (g_object_ref_sink (panel)),
                        gtd_panel_get_panel_name (panel),
                        gtd_panel_get_panel_title (panel));

  g_signal_emit (self, signals[PANEL_ADDED], 0, panel);
}

static void
remove_panel (gpointer data,
              gpointer user_data)
{
  GtdPanel *panel = GTD_PANEL (data);
  GtdWindow *self = GTD_WINDOW (user_data);
  g_object_ref (panel);

  gtk_stack_remove (self->stack, GTK_WIDGET (panel));
  g_signal_emit (self, signals[PANEL_REMOVED], 0, panel);

  g_object_unref (panel);
}

static void
update_panel_menu (GtdWindow *self)
{
  GtkPopover *popover;
  const GMenu *menu;

  popover = gtd_panel_get_popover (self->active_panel);
  menu = gtd_panel_get_menu (self->active_panel);

  gtk_widget_set_visible (GTK_WIDGET (self->gear_menu_button), popover || menu);

  if (popover)
    {
      gtk_menu_button_set_popover (self->gear_menu_button, GTK_WIDGET (popover));
    }
  else
    {
      gtk_menu_button_set_popover (self->gear_menu_button, NULL);
      gtk_menu_button_set_menu_model (self->gear_menu_button, G_MENU_MODEL (menu));
    }
}

/*
 * Callbacks
 */
static void
on_action_activate_panel_activated_cb (GSimpleAction *simple,
                                       GVariant      *parameters,
                                       gpointer       user_data)
{
  GtdWindow *self = GTD_WINDOW (user_data);
  g_autoptr (GVariant) panel_parameters = NULL;
  g_autofree gchar *panel_id = NULL;
  GtdPanel *panel;

  g_variant_get (parameters,
                 "(sv)",
                 &panel_id,
                 &panel_parameters);

  g_debug ("Activating panel '%s'", panel_id);

  panel = (GtdPanel *) gtk_stack_get_child_by_name (self->stack, panel_id);
  g_return_if_fail (panel && GTD_IS_PANEL (panel));

  gtd_panel_activate (panel, panel_parameters);

  gtk_stack_set_visible_child (self->stack, GTK_WIDGET (panel));
  adw_leaflet_navigate (self->leaflet, ADW_NAVIGATION_DIRECTION_FORWARD);
}

static void
on_action_toggle_archive_activated_cb (GSimpleAction *simple,
                                       GVariant      *state,
                                       gpointer       user_data)
{
  GtdWindow *self = GTD_WINDOW (user_data);
  gboolean archive_visible = g_variant_get_boolean (state);

  gtk_widget_set_visible (self->new_list_button, !archive_visible);
  gtd_sidebar_set_archive_visible (self->sidebar, archive_visible);
}

static void
on_back_sidebar_button_clicked_cb (GtkButton *button,
                                   GtdWindow *self)
{
  adw_leaflet_navigate (self->leaflet, ADW_NAVIGATION_DIRECTION_BACK);
}

static void
on_back_button_clicked_cb (GtkButton *button,
                           GtdWindow *self)
{
  gtk_widget_activate_action (GTK_WIDGET (self),
                              "win.toggle-archive",
                              "b",
                              FALSE);
}

static void
on_panel_menu_changed_cb (GObject    *object,
                          GParamSpec *pspec,
                          GtdWindow  *self)
{
  if (GTD_PANEL (object) != self->active_panel)
    return;

  update_panel_menu (self);
}

static void
on_stack_visible_child_cb (GtdWindow  *self,
                           GParamSpec *pspec,
                           GtkStack   *stack)
{
  GtkWidget *visible_child;
  GtdPanel *panel;

  GTD_ENTRY;

  visible_child = gtk_stack_get_visible_child (stack);
  panel = GTD_PANEL (visible_child);

  g_signal_connect (panel, "notify::menu", G_CALLBACK (on_panel_menu_changed_cb), self);

  /* Set panel as the new active panel */
  g_set_object (&self->active_panel, panel);

  /* Setup the panel's menu */
  update_panel_menu (self);

  GTD_EXIT;
}

static void
on_toast_dismissed_cb (AdwToast *self,
                       gpointer  user_data)
{
  GtdNotification *notification = user_data;

  gtd_notification_execute_dismissal_action (notification);
}

static void
toast_activated_cb (GtdWindow  *self,
                    const char *action_name,
                    GVariant   *parameter)
{
  AdwToast *toast;
  GtdNotification *notification;

  GTD_ENTRY;

  toast = g_hash_table_lookup (self->notification_list, g_variant_get_string (parameter, NULL));

  if (toast != NULL) {
    notification = g_object_get_data (G_OBJECT (toast), "notification");

    g_signal_handlers_block_by_func (toast, on_toast_dismissed_cb, notification);
    gtd_notification_execute_secondary_action (notification);
  }

  GTD_EXIT;
}

static void
on_show_notification_cb (GtdManager      *manager,
                         GtdNotification *notification,
                         GtdWindow       *self)
{
  AdwToast *toast;
  GValue btn_label = G_VALUE_INIT;

  g_object_get_property (G_OBJECT (notification), "secondary-action-name", &btn_label);

  /* Convert GtdNotification to AdwToast */
  toast = adw_toast_new (gtd_notification_get_text (notification));
  adw_toast_set_button_label (toast, g_value_get_string (&btn_label));
  adw_toast_set_action_name (toast, "toast.activated");
  adw_toast_set_action_target_value (toast, g_variant_new_string (g_value_get_string (&btn_label)));
  g_object_set_data (G_OBJECT (toast), "notification", notification);

  g_hash_table_insert (self->notification_list, (char *) g_value_get_string (&btn_label), toast);
  g_signal_connect (toast, "dismissed", G_CALLBACK (on_toast_dismissed_cb), notification);

  adw_toast_overlay_add_toast (self->notification_overlay, toast);
}

static void
error_message_notification_primary_action (GtdNotification *notification,
                                           gpointer         user_data)
{
  error_data_free (user_data);
}

static void
error_message_notification_secondary_action (GtdNotification *notification,
                                             gpointer         user_data)
{
  GtkWidget *dialog;
  ErrorData *data;

  data = user_data;
  dialog = adw_message_dialog_new (GTK_WINDOW (data->window),
                                   data->primary_text,
                                   NULL);

  adw_message_dialog_format_body (ADW_MESSAGE_DIALOG (dialog),
                                  "%s",
                                  data->secondary_text);

  adw_message_dialog_add_response (ADW_MESSAGE_DIALOG (dialog),
                                   "close", _("Close"));

  g_signal_connect (dialog,
                    "response",
                    G_CALLBACK (gtk_window_destroy),
                    NULL);

  gtk_widget_set_visible (dialog, TRUE);

  error_data_free (data);
}

static void
on_show_error_message_cb (GtdManager                *manager,
                          const gchar               *primary_text,
                          const gchar               *secondary_text,
                          GtdNotificationActionFunc  function,
                          gpointer                   user_data,
                          GtdWindow                 *self)
{
  GtdNotification *notification;
  ErrorData *error_data;

  error_data = g_new0 (ErrorData, 1);
  notification = gtd_notification_new (primary_text);

  error_data->window = GTD_WINDOW (gtk_widget_get_root (GTK_WIDGET (self)));
  error_data->primary_text = g_strdup (primary_text);
  error_data->secondary_text = g_strdup (secondary_text);

  gtd_notification_set_dismissal_action (notification,
                                         error_message_notification_primary_action,
                                         error_data);

  if (!function)
    {
      gtd_notification_set_secondary_action (notification,
                                             _("Details"),
                                             error_message_notification_secondary_action,
                                             error_data);
    }
  else
    {
      gtd_notification_set_secondary_action (notification, secondary_text, function, user_data);
    }

  gtd_manager_send_notification (gtd_manager_get_default (), notification);
}

/*
 * GtkWindow overrides
 */

static void
gtd_window_unmap (GtkWidget *widget)
{
  GSettings *settings;
  GtkWindow *window;
  gboolean maximized;

  window = GTK_WINDOW (widget);
  settings = gtd_manager_get_settings (gtd_manager_get_default ());
  maximized = gtk_window_is_maximized (window);

  g_settings_set_boolean (settings, "window-maximized", maximized);

  if (!maximized)
    {
      gint height;
      gint width;

      gtk_window_get_default_size (window, &width, &height);
      g_settings_set (settings, "window-size", "(ii)", width, height);
    }

  GTK_WIDGET_CLASS (gtd_window_parent_class)->unmap (widget);
}

/*
 * GObject overrides
 */

static void
gtd_window_dispose (GObject *object)
{
  GtdWindow *self = GTD_WINDOW (object);

  g_list_foreach (self->panels_set, remove_panel, self);
  g_list_free (g_steal_pointer (&self->panels_set));

  G_OBJECT_CLASS (gtd_window_parent_class)->dispose (object);
}

static void
gtd_window_constructed (GObject *object)
{
  GtdWindow *self = GTD_WINDOW (object);
  GtdManager *manager = gtd_manager_get_default ();

  G_OBJECT_CLASS (gtd_window_parent_class)->constructed (object);

  /* Create default panels */
  self->panels_set = g_list_append (self->panels_set, GTD_PANEL (gtd_panel_all_tasks_new ()));
  self->panels_set = g_list_append (self->panels_set, GTD_PANEL (gtd_panel_inbox_new ()));
  self->panels_set = g_list_append (self->panels_set, GTD_PANEL (gtd_panel_next_week_new ()));
  self->panels_set = g_list_append (self->panels_set, GTD_PANEL (gtd_panel_today_new ()));

  g_list_foreach (self->panels_set, add_panel, self);

  gtd_sidebar_activate (self->sidebar);

  /* Setup manager */
  g_signal_connect (manager, "show-notification", G_CALLBACK (on_show_notification_cb), self);
  g_signal_connect (manager, "show-error-message", G_CALLBACK (on_show_error_message_cb), self);

  /* Load stored size */
  load_geometry (GTD_WINDOW (object));
}

static void
gtd_window_class_init (GtdWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = gtd_window_dispose;
  object_class->constructed = gtd_window_constructed;

  widget_class->unmap = gtd_window_unmap;

  /**
   * GtdWindow::panel-added:
   * @self: a #GtdWindow
   * @panel: a #GtdPanel
   *
   * The ::panel-added signal is emitted after a #GtdPanel
   * is added.
   */
  signals[PANEL_ADDED] = g_signal_new ("panel-added",
                                        GTD_TYPE_WINDOW,
                                        G_SIGNAL_RUN_LAST,
                                        0,
                                        NULL,
                                        NULL,
                                        NULL,
                                        G_TYPE_NONE,
                                        1,
                                        GTD_TYPE_PANEL);

  /**
   * GtdWindow::panel-removed:
   * @self: a #GtdWindow
   * @panel: a #GtdPanel
   *
   * The ::panel-removed signal is emitted after a #GtdPanel
   * is removed from the list.
   */
  signals[PANEL_REMOVED] = g_signal_new ("panel-removed",
                                         GTD_TYPE_WINDOW,
                                         G_SIGNAL_RUN_LAST,
                                         0,
                                         NULL,
                                         NULL,
                                         NULL,
                                         G_TYPE_NONE,
                                         1,
                                         GTD_TYPE_PANEL);

  g_type_ensure (GTD_TYPE_PROVIDER_POPOVER);
  g_type_ensure (GTD_TYPE_SIDEBAR);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/todo/ui/gtd-window.ui");

  gtk_widget_class_bind_template_child (widget_class, GtdWindow, back_button);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, content_box);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, gear_menu_button);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, leaflet);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, new_list_button);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, primary_menu_button);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, sidebar);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, sidebar_box);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, GtdWindow, notification_overlay);

  gtk_widget_class_bind_template_callback (widget_class, on_back_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_back_sidebar_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_stack_visible_child_cb);

  gtk_widget_class_install_action (widget_class, "toast.activated", "s", (GtkWidgetActionActivateFunc) toast_activated_cb);
}

static void
gtd_window_init (GtdWindow *self)
{
  GtkApplication *application;
  GMenu *primary_menu;

  static const GActionEntry entries[] = {
    { "activate-panel", on_action_activate_panel_activated_cb, "(sv)" },
    { "toggle-archive", on_action_toggle_archive_activated_cb, "b" },
  };

  gtk_widget_init_template (GTK_WIDGET (self));

  self->notification_list = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   entries,
                                   G_N_ELEMENTS (entries),
                                   self);

  gtk_actionable_set_action_target_value (GTK_ACTIONABLE (self->back_button),
                                          g_variant_new_boolean (FALSE));

  /* Task list panel */
  self->task_list_panel = GTD_PANEL (gtd_panel_task_list_new ());
  add_panel (self->task_list_panel, self);

  gtd_sidebar_connect (self->sidebar, GTK_WIDGET (self));
  gtd_sidebar_set_panel_stack (self->sidebar, self->stack);
  gtd_sidebar_set_task_list_panel (self->sidebar, self->task_list_panel);

  /* Fancy primary menu */
  application = GTK_APPLICATION (g_application_get_default ());
  primary_menu = gtk_application_get_menu_by_id (application, "primary-menu");
  gtk_menu_button_set_menu_model (self->primary_menu_button, G_MENU_MODEL (primary_menu));

  /* Development build */
  if (is_development_build ())
    setup_development_build (self);
}

GtkWidget*
gtd_window_new (GtdApplication *application)
{
  return g_object_new (GTD_TYPE_WINDOW,
                       "application", application,
                       NULL);
}
